var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('login', { title: 'Login' });
});

router.post('/', function (req, res, next) {
	// you might like to do a database look-up or something more scalable here
	if (req.body.username && req.body.username === 'deep' && req.body.password && req.body.password === '123456') {
		req.session.authenticated = true;
		req.session.user = req.body.username;
		res.redirect('/users');
	} else {
		req.flash('error', 'Username and password are incorrect');
		res.redirect('/login');
	}

});

module.exports = router;